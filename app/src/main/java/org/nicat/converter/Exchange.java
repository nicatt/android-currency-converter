package org.nicat.converter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.nicat.converter.lib.AsyncTaskInterface;
import org.nicat.converter.lib.Currencies;
import org.nicat.converter.lib.CustomAdaptor;
import org.nicat.converter.lib.DB;
import org.nicat.converter.lib.GetXMLFromServer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Exchange extends Fragment implements AsyncTaskInterface {

    private List<Currencies> allCurrencies;
    private String date;

    Exchange $class = this;
    Context context;
    View rootView;
    ListView listView;
    TextView currencyDate, txtGet;
    ConstraintLayout exchangeLayout;
    SearchView searchView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        init(inflater, container);

        LoadCurrencies();

        txtGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadCurrencies();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Currencies> filteredList = new ArrayList<>();
                newText = newText.toLowerCase();

                for (int i = 0; i < allCurrencies.size(); i++){
                    Currencies tmp = allCurrencies.get(i);
                    String code = tmp.getCode().toLowerCase();
                    String desc = tmp.getDescription().toLowerCase();
                    String value = tmp.getValue().toLowerCase();

                    if(code.contains(newText) || desc.contains(newText) || value.contains(newText)){
                        filteredList.add(tmp);
                    }
                }

                if(filteredList.size() <= 0){
                    Toast.makeText(context, R.string.not_found, Toast.LENGTH_SHORT).show();
                }

                CustomAdaptor customAdaptor = new CustomAdaptor(context, filteredList);
                listView.setAdapter(customAdaptor);
                
                return false;
            }
        });

        currencyDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] Date = date.split("\\.");

                Calendar c = Calendar.getInstance();
                int year  = Integer.parseInt(Date[2]);
                int month = Integer.parseInt(Date[1]);
                int day = Integer.parseInt(Date[0]);

                DatePickerDialog dialog = new DatePickerDialog(context, R.style.DatePickerDialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month+1;

                        String day = String.valueOf(dayOfMonth);
                        String Month = String.valueOf(month);

                        day = day.length() == 1 ? "0"+day : day;
                        Month = Month.length() == 1 ? "0"+Month : Month;

                        date = day+"."+Month+"."+year;

                        LoadCurrencies();
                    }
                }, year, month, day);


                try{
                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                    String dateInString = "25.11.1993";
                    Date date = sdf.parse(dateInString);

                    dialog.getDatePicker().setMinDate(date.getTime());
                } catch (Exception e){

                }

                dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.select), dialog);
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), dialog);
                dialog.show();
            }
        });

        return rootView;
    }

    private void init(LayoutInflater inflater, ViewGroup container){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        date = df.format(c);

        rootView = inflater.inflate(R.layout.exchange, container, false);

        listView = rootView.findViewById(R.id.lstCurrencies);
        exchangeLayout = rootView.findViewById(R.id.exchangeLayout);
        searchView = rootView.findViewById(R.id.searchView);
        currencyDate = rootView.findViewById(R.id.txtDate);
        txtGet = rootView.findViewById(R.id.txtGet);
        context = rootView.getContext();
    }

    private void LoadCurrencies(){
        GetXMLFromServer xml = new GetXMLFromServer(date);
        xml.delegate = $class;
        xml.execute();

        searchView.setQuery(null, false);
        currencyDate.setText(getString(R.string.date, date));
    }

    @Override
    public void processFinish(String output) {
        if(output.equals("NotConnected")){
            Snackbar snackbar = Snackbar
                    .make(exchangeLayout, R.string.connection_error, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
        else{
            FillList(output);
        }
    }

    private void FillList(String output){
        ParseXML(output);

        List<Currencies> currencies = new ArrayList<>();
        DB db = new DB(context);
        db.onUpgrade(db.getWritableDatabase(), 1, 2);

        for (int i = 0; i < allCurrencies.size(); i++){
            Currencies tmp = allCurrencies.get(i);

            String val = tmp.getValue();

            if((val != null && val.length() > 0) && !val.equals("0")){
                currencies.add(tmp);

                db.addCurrency(tmp);
            }
        }

        allCurrencies = currencies;
        db.getCurrencies();
        CustomAdaptor customAdaptor = new CustomAdaptor(context, allCurrencies);
        listView.setAdapter(customAdaptor);
    }

    private void ParseXML(String xmlString){

        List<Currencies> allCurrencies = new ArrayList<>();
        Currencies currencies = new Currencies();

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(xmlString));
            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT){
                String name = parser.getName();
                if(eventType== XmlPullParser.START_TAG){
                    if(name.equals("ValCurs")){
                        date = parser.getAttributeValue(null,"Date");
                    }
                    else if(name.equals("Valute")){
                        String Code = parser.getAttributeValue(null,"Code");
                        currencies.setCode(Code);
                    }
                    else if(name.equals("Nominal")) {

                        if(parser.next() == XmlPullParser.TEXT) {
                            String Nominal = parser.getText();
                            currencies.setNominal(Nominal);
                        }
                    }
                    else if(name.equals("Name")) {

                        if(parser.next() == XmlPullParser.TEXT) {
                            String Name = parser.getText();
                            currencies.setDescription(Name);
                        }
                    }
                    else if(name.equals("Value")) {

                        if(parser.next() == XmlPullParser.TEXT) {
                            String Value = parser.getText();
                            currencies.setValue(Value);
                        }
                    }
                }
                else if(eventType== XmlPullParser.END_TAG){
                    if(name.equals("Valute")){
                        allCurrencies.add(currencies);
                        currencies = new Currencies();
                    }
                }
                eventType = parser.next();
            }

            this.allCurrencies = allCurrencies;

        }catch (Exception e){
        }

    }
}
