package org.nicat.converter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Converter extends Fragment {

    TextView txtNumber;
    Button btnDot, btnDel;
    Spinner spinnerFrom;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.converter, container, false);

        txtNumber = rootView.findViewById(R.id.txtNumber);
        spinnerFrom = rootView.findViewById(R.id.spinnerFrom);
        btnDot = rootView.findViewById(R.id.btnDot);
        btnDel = rootView.findViewById(R.id.btnDel);

        btnDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String current = (String) txtNumber.getText();

                if(current.isEmpty()){
                    txtNumber.setText("0,");
                }else if(!current.contains(",")){
                    txtNumber.setText(current + ",");
                }else{
                    //do nothing
                }


            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String current = (String) txtNumber.getText();

                if(current.length() > 1){
                    current = current.substring(0, current.length() - 1);
                }
                else if(!current.equals("0")){
                    current = "0";
                }else{
                    //do nothing
                }

                txtNumber.setText(current);
            }
        });

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("AZN");
        spinnerArray.add("TRY");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                rootView.getContext(), android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFrom.setAdapter(adapter);

        return rootView;
    }

    public void numberClick(View v){
        switch (v.getId()){
            case R.id.btn0:
                editText(0);
                break;
            case R.id.btn1:
                editText(1);
                break;
            case R.id.btn2:
                editText(2);
                break;
            case R.id.btn3:
                editText(3);
                break;
            case R.id.btn4:
                editText(4);
                break;
            case R.id.btn5:
                editText(5);
                break;
            case R.id.btn6:
                editText(6);
                break;
            case R.id.btn7:
                editText(7);
                break;
            case R.id.btn8:
                editText(8);
                break;
            case R.id.btn9:
                editText(9);
                break;
        }
    }

    private void editText(int num){
        String curr = (String) txtNumber.getText();
        curr = curr.equals("0") ? "" : curr;
        txtNumber.setText(curr+num);
    }
}
