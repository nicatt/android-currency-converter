package org.nicat.converter.lib;

import android.os.AsyncTask;

import java.io.InputStream;

public class GetXMLFromServer extends AsyncTask<String,Void,String> {
    private HttpHandler nh;
    private String date;
    public AsyncTaskInterface delegate = null;

    public GetXMLFromServer(String date) {
        this.date = date;
    }

    @Override
    protected String doInBackground(String... strings) {

        String URL = "http://cbar.az/currencies/"+date+".xml";
        String res = "";
        nh =  new HttpHandler();
        InputStream is = nh.CallServer(URL);
        if(is!=null){
            res = nh.StreamToString(is);
        }
        else{
            res = "NotConnected";
        }

        return res;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        delegate.processFinish(result);
    }
}
