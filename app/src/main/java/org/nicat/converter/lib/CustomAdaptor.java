package org.nicat.converter.lib;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.nicat.converter.R;

import java.util.List;

public class CustomAdaptor extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Currencies> currencies;

    public CustomAdaptor(Context context, List<Currencies> currencies) {
        this.context = context;
        this.currencies = currencies;
    }

    @Override
    public int getCount() {
        return currencies.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = LayoutInflater.from(context);

        View row = layoutInflater.inflate(R.layout.currency, null);

        ImageView imgFlag = row.findViewById(R.id.imgFlag);
        TextView txtValue = row.findViewById(R.id.txtValue);
        TextView txtDescription = row.findViewById(R.id.txtDescription);

        Currencies currency = currencies.get(position);
        Flags flags = new Flags();

        try{
            imgFlag.setImageResource(flags.get(currency.getCode()));
        }
        catch (Exception e){
            imgFlag.setImageResource(flags.get("Unknown"));
        }
        txtDescription.setText(currency.getNominal() + " " + currency.getDescription());
        txtValue.setText(currency.getValue());
        
        return row;
    }
}
