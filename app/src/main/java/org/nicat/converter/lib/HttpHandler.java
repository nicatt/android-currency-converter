package org.nicat.converter.lib;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpHandler {

    public InputStream CallServer(String remoteURL) {
        InputStream inStm;

        try {
            URL url = new URL(remoteURL);
            HttpURLConnection cc = (HttpURLConnection) url.openConnection();
            cc.setReadTimeout(5000);
            cc.setConnectTimeout(5000);
            cc.setRequestMethod("GET");
            cc.setDoInput(true);
            int response = cc.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                inStm = cc.getInputStream();
            }
            else{
                inStm = null;
            }

        } catch (Exception e) {
            Log.e("httpExc", "er", e);
            inStm = null;
        }

        return inStm;

    }

    public String StreamToString(InputStream stream) {
        InputStreamReader isr = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder response = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

            stream.close();
        }
        catch (Exception e) {
            Log.e("strTostr", "er", e);
        }

        return response.toString();
    }
}