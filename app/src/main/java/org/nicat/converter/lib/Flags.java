package org.nicat.converter.lib;

import android.view.View;

import org.nicat.converter.R;

import java.util.HashMap;
import java.util.Map;

public class Flags {
    private Map<String, Integer> list = new HashMap<>();

    public Flags() {
        list.put("Unknown", R.drawable.ic_list_unknown);
        list.put("XPD", R.drawable.ic_list_az);
        list.put("XPT", R.drawable.ic_list_az);
        list.put("XAG", R.drawable.ic_list_az);
        list.put("XAU", R.drawable.ic_list_az);
        list.put("CZK", R.drawable.ic_list_cz);
        list.put("IRR", R.drawable.ic_list_ir);
        list.put("KRW", R.drawable.ic_list_kr);
        list.put("TMT", R.drawable.ic_list_tm);
        list.put("RUB", R.drawable.ic_list_ru);
        list.put("GEL", R.drawable.ic_list_ge);
        list.put("NZD", R.drawable.ic_list_nz);
        list.put("KZT", R.drawable.ic_list_kz);
        list.put("NOK", R.drawable.ic_list_no);
        list.put("GBP", R.drawable.ic_list_gb);
        list.put("BYN", R.drawable.ic_list_by);
        list.put("TRY", R.drawable.ic_list_tr);
        list.put("EGP", R.drawable.ic_list_eg);
        list.put("LBP", R.drawable.ic_list_lb);
        list.put("KWD", R.drawable.ic_list_kw);
        list.put("UZS", R.drawable.ic_list_uz);
        list.put("SEK", R.drawable.ic_list_se);
        list.put("CNY", R.drawable.ic_list_cn);
        list.put("TWD", R.drawable.ic_list_tw);
        list.put("CLP", R.drawable.ic_list_cl);
        list.put("IDR", R.drawable.ic_list_id);
        list.put("INR", R.drawable.ic_list_in);
        list.put("KGS", R.drawable.ic_list_kg);
        list.put("CAD", R.drawable.ic_list_ca);
        list.put("MYR", R.drawable.ic_list_my);
        list.put("TJS", R.drawable.ic_list_tj);
        list.put("MDL", R.drawable.ic_list_md);
        list.put("ILS", R.drawable.ic_list_il);
        list.put("SGD", R.drawable.ic_list_sg);
        list.put("SDR", R.drawable.ic_list_sd);
        list.put("JPY", R.drawable.ic_list_jp);
        list.put("PLN", R.drawable.ic_list_pl);
        list.put("AUD", R.drawable.ic_list_au);
        list.put("SAR", R.drawable.ic_list_sa);
        list.put("BRL", R.drawable.ic_list_br);
        list.put("ARS", R.drawable.ic_list_ar);
        list.put("UAH", R.drawable.ic_list_ua);
        list.put("DKK", R.drawable.ic_list_dk);
        list.put("EUR", R.drawable.ic_list_eu);
        list.put("MXN", R.drawable.ic_list_mx);
        list.put("ZAR", R.drawable.ic_list_za);
        list.put("CHF", R.drawable.ic_list_ch);
        list.put("HKD", R.drawable.ic_list_hk);
        list.put("AED", R.drawable.ic_list_ae);
        list.put("USD", R.drawable.ic_list_us);
    }

    public Integer get(String code){
        return list.get(code);
    }
}
