package org.nicat.converter.lib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DB extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "converter";

    public DB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE `currencies` (" +
                   "`currency_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
                   "`currency_code` TEXT UNIQUE," +
                   "`currency_name` TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS currencies;");
        this.onCreate(db);
    }

    public void addCurrency(Currencies currency){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("currency_code", currency.getCode());
        values.put("currency_name", currency.getDescription());

        db.insert("currencies", null, values);
    }

    public List<Currencies> getCurrencies(){
        String query = "SELECT * FROM currencies;";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                Log.e("ID", cursor.getString(0));
                Log.e("CODE", cursor.getString(1));
                Log.e("NAME", cursor.getString(2));
            }while (cursor.moveToNext());
        }

        List<Currencies> r = new ArrayList<>();
        return r;
    }
}
