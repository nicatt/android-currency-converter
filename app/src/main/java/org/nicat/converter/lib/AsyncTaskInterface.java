package org.nicat.converter.lib;

public interface AsyncTaskInterface {
    void processFinish(String output);
}
